const { Console } = require('console');
const fs = require('fs');
const { memoryUsage } = require('process');

const fileRead = fs.readFileSync('./Day2/input.txt', 'utf8')
const scanResults = fileRead.split('\r\n');

let depth = 0;
let speed = 0;

scanResults.forEach((scanResult, i) => {
    let [direction, increment] = scanResult.split(' ');
    increment = parseInt(increment);
    switch (direction) {
        case 'forward':
            speed += increment;
            break;
        case 'up':
            depth -= increment;
            break;
        case 'down':
            depth += increment;
            break;
    }
})

console.log("Answer: ", depth * speed);

