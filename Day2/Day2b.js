const { Console } = require('console');
const fs = require('fs');

const fileRead = fs.readFileSync('./Day2/input.txt', 'utf8')
const scanResults = fileRead.split('\r\n');

let depth = 0;
let speed = 0;
let aim = 0;
scanResults.forEach((scanResult) => {
    let [direction, increment] = scanResult.split(' ');
    increment = parseInt(increment);
    switch (direction) {
        case 'forward':
            speed += increment;
            depth += increment * aim;
            break;
        case 'up':
            aim -= increment
            break;
        case 'down':
            aim += increment
            break;
    }
})

console.log("Answer: ", depth * speed);