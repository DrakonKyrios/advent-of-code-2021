const fs = require('fs');

const fileRead = fs.readFileSync('./Day1/input.txt', 'utf8')
const scanResults = fileRead.split('\r\n');

let answer = 0;
for (let i = 1; i < scanResults.length; i++) {
    if (parseInt(scanResults[i]) > parseInt(scanResults[i - 1])) {
        answer++;
    }
}
console.log(answer);