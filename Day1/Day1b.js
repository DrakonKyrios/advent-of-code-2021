const fs = require('fs');

const fileRead = fs.readFileSync('./input.txt', 'utf8')
let scanResults = fileRead.split('\r\n');
scanResults = scanResults.map((scanResult) => parseInt(scanResult));

let answer = 0;

const previousThreeSum = (scanArray) => {
    return scanArray.reduce((mem, current) => mem + current, 0);
}

for (let i = 2; i < scanResults.length; i++) {
    var first = previousThreeSum(scanResults.slice(i - 2, i + 1));
    var second = previousThreeSum(scanResults.slice(i - 1, i + 2));
    if (second > first) {
        answer++;
    }
}
console.log(answer);