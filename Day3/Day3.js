const { strictEqual } = require('assert');
const fs = require('fs');

const fileRead = fs.readFileSync('./Day3/input.txt', 'utf8')
const scanResults = fileRead.split('\r\n');

let gamma = 0;
let epsilon = 0;
let decodedBits = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

for (let i = 0; i < scanResults.length; i++) {
    let scanResult = scanResults[i];
    for (let b = 0; b < scanResult.length; b++) {
        if (scanResult[b] === '1') {
            decodedBits[b]++;
        } else {
            decodedBits[b]--;
        }
    }
}

while (decodedBits.length > 0) {
    let bit = decodedBits.shift();
    let theoriticalValue = Math.pow(2, decodedBits.length);
    if (bit > 0) {
        gamma += theoriticalValue;
    } else {
        epsilon += theoriticalValue;
    }
}

console.log("Answer:", gamma * epsilon);
