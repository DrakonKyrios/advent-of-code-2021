const fs = require('fs');

const fileRead = fs.readFileSync('./Day3/input.txt', 'utf8')
const scanResults = fileRead.split('\r\n');

const bitMajorityByIndicator = (results, searchIndex, majorityIndicator) => {
    let majorityScale = 0;
    for (let i = 0; i < results.length; i++) {
        let scanResult = results[i];
        if (scanResult[searchIndex] === majorityIndicator) {
            majorityScale++;
        } else {
            majorityScale--;
        }
    }

    return majorityScale >= 0
}

const byteValue = (byteString) => {
    let accumlatedValue = 0;
    let byteArray = byteString.split('');

    while (byteArray.length > 0) {
        let bit = byteArray.shift();
        let bitValue = Math.pow(2, byteArray.length);
        if (bit > 0) {
            accumlatedValue += bitValue;
        }
    }

    return accumlatedValue;
}

const searchResultsByMajorityIndicator = (results, isMajoritySearch) => {
    let searchIndex = 0;
    while (results.length > 1) {
        const majorityAtIsOne = bitMajorityByIndicator(results, searchIndex, "1");

        results = results.filter((result) => {
            const resultAtIsOne = result[searchIndex] === "1";
            if (resultAtIsOne === majorityAtIsOne) {
                return isMajoritySearch;
            }

            return !isMajoritySearch;
        });

        searchIndex++;
    }

    return results;
}

const oxygenCandidates = searchResultsByMajorityIndicator([...scanResults], true);
const co2Candidates = searchResultsByMajorityIndicator([...scanResults], false);

if (oxygenCandidates.length <= 0) {
    console.log(" NO Oxygen Rating");
}

if (co2Candidates.length <= 0) {
    console.log(" NO CO2 Rating");
}

const oxygenRating = byteValue(oxygenCandidates[0]);
const co2Rating = byteValue(co2Candidates[0]);

console.log("Answer: ", oxygenRating * co2Rating)

// 6822109